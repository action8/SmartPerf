/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * GPData实体类
 */
export class GPData {
    public moduleType: String
    public timeStamp: number
    public values: Map<String, String>

    constructor(moduleType: String, timeStamp: number, values: Map<String, String>) {
        this.moduleType = moduleType;
        this.timeStamp = timeStamp;
        this.values = values;
    }

    toString() {
        var obj = Object.create(null);
        var iterator = this.values.keys();
        for (var i = 0; i < this.values.size; i++) {
            var key = iterator.next().value;
            obj[key] = this.values.get(key);
        }
        return "GPData{" +
        "module='" + this.moduleType + '\'' +
        ", timeStamp=" + this.timeStamp +
        ", values=" + JSON.stringify(obj) +
        '}';
    }
}

/**
 * 任务列表页  任务概览页实体类
 */
export class TGeneralInfo {
    //目录生成的UUID  作为主键
    public sessionId: String
    //测试游戏名称
    public appName: String
    //测试游戏版本
    public appVersion: String
    //当前电池电量
    public batteryVolume: String
    //设备型号
    public board: String
    //设备品牌
    public brand: String
    //大中小核心
    public cpuCluster: String
    //报告来源
    public dataSource: String
    //设备名称  XX-XXX(****00018)
    public deviceName: String
    //设备类型
    public deviceTypeName: String
    //结束时间
    public endTime: number
    //游戏包名
    public packageName: String
    //芯片平台
    public plat: String
    //项目Id
    public projectId: String
    //分辨类
    public resolution: String
    //当前亮度
    public screenBrightness: String
    //设备sn号
    public sn: String
    //sp版本
    public spVersion: String
    //开始时间
    public startTime: number
    //任务Id
    public taskId: String
    //任务名称
    public taskName: String
    //测试方式  middle
    public testCase: String
    // 有效时间
    public validTimes: number
    //测试类型  smartPerf beta
    public testType: String
    public upStatus: String
    public uploadTaskId: String
    //用户名 id
    public user: String
    // 用户名
    public userName: String
    //手机版本
    public version: String
    //设备当前音量
    public volume: String
    public test: String
    // OH only
    //测试时长
    public testDuration: number
    //用户id
    public userId: String
    //是否是多路测试
    public isOnline: boolean
    //设备号
    public deviceNum: number
    //房间号
    public roomNumber: String
    //任务类型 主控、被控
    public taskType: number

    constructor(sessionId ?: String, taskId ?: String, appName ?: String, appVersion ?: String, packageName ?: String, startTime ?: number, endTime ?: number, testDuration ?: number, taskName ?: String,
                upStatus?: String, deviceName?: String, cpuCluster?: String, uploadTaskId?: String) {
        this.sessionId = sessionId
        this.appName = appName
        this.appVersion = appVersion
        this.batteryVolume = '0%'
        this.board = "OpenHarmony"
        this.brand = "HUAWEI"
        this.cpuCluster = cpuCluster
        this.dataSource = "SmartPerf"
        this.deviceName = deviceName
        this.deviceTypeName = "手机"
        this.endTime = endTime
        this.packageName = packageName
        this.plat = "N/A"
        this.projectId = ""
        this.resolution = "N/A"
        this.screenBrightness = "N/A"
        this.sn = "1234567890"
        this.spVersion = "N/A"
        this.startTime = startTime
        this.taskId = taskId
        this.taskName = taskName
        this.testCase = 'middle'
        this.validTimes = testDuration * 1000
        this.testType = "manual"
        this.upStatus = upStatus
        this.uploadTaskId = uploadTaskId
        this.user = ""
        this.userName = ""
        this.version = ""
        this.volume = "N/A"
        this.test = "N/A"
        this.testDuration = testDuration
    }
}

/**
 * 任务详情实体类
 */
export class TIndexInfo {
    //时间戳 主键
    public timestamp: String;
    //任务ID
    public taskId: String;
    //温度
    public ambientTemp: String;
    public shellBackTemp: String;
    public socThermalTemp: String;
    public batteryTemp: String;
    public gpuTemp: String;
    public shellFrontTemp: String;
    public shellFrameTemp: String;
    public systemHTemp: String;
    //ddr
    public ddrFrequency: String;
    //fps
    public fps: String;
    public fpsJitters: String;
    public jank: String;
    //场景标识
    public flag: String;
    // 卡顿次数
    public caton: number;
    public generalCatonNum: number;
    public criticalCatonNum: number;
    public fatalCatonNum: number;
    //cpu
    public cpu0Frequency: String;
    public cpu0Load: String;
    public cpu1Frequency: String;
    public cpu1Load: String;
    public cpu2Frequency: String;
    public cpu2Load: String;
    public cpu3Frequency: String;
    public cpu3Load: String;
    public cpu4Frequency: String;
    public cpu4Load: String;
    public cpu5Frequency: String;
    public cpu5Load: String;
    public cpu6Frequency: String;
    public cpu6Load: String;
    public cpu7Frequency: String;
    public cpu7Load: String;
    public cpu8Frequency: String;
    public cpu8Load: String;
    public cpu9Frequency: String;
    public cpu9Load: String;
    public cpu10Frequency: String;
    public cpu10Load: String;
    public cpu11Frequency: String;
    public cpu11Load: String;
    public cpuLoad: String;
    //gpu
    public gpuFrequency: String;
    public gpuLoad: String;
    //power
    public currentNow: String;
    public capacity: String;
    public enableHiz: String;
    public status: String;
    public voltageNow: String;
    //ram
    public pss: String;
    public cpu1ClusterFre: String
    public cpu2ClusterFre: String
    public cpu3ClusterFre: String
    //HWCPipeGPU
    public cacheMisses: String;
    public instructions: String;
    //HWCPipeGPU
    public gpuCycles: String;
    public vertexComputeCycles: String;
    public fragmentCycles: String;
    public tilerCycles: String;
    public vertexComputeJobs: String;
    public fragmentJobs: String;
    public pixels: String;
    public earlyZTests: String;
    public earlyZKilled: String;
    public externalMemoryReadAccesses: String;
    public externalMemoryWriteAccesses: String;
    public externalMemoryReadBytes: String;
    public externalMemoryWriteBytes: String;
    public cacheWriteLookups: String;
    public cacheReadLookups: String;
    public externalMemoryWriteStalls: String;
    public externalMemoryReadStalls: String;
    public shaderCycles: String;
    public shaderArithmeticCycles: String;
    public shaderLoadStoreCycles: String;
    public shaderTextureCycles: String;

    //QPCounters
    public clocksSecond: String;
    public gpuUtilization: String;
    public gpuBusBusy: String;
    public verticesShadedSecond: String;
    public fragmentsShadedSecond: String;
    public texturesVertex: String;
    public texturesFragment: String;
    public aluVertex: String;
    public aluFragment: String;
    public timeShadingFragments: String;
    public timeShadingVertices: String;
    public timeCompute: String;
    public readTotal: String;
    public writeTotal: String;
    public textureMemoryReadBW: String;
    public vertexMemoryRead: String;
    public spMemoryRead: String;
    public qpGPUFrequency: String;
    //network
    public currNetworkType: String;
    public networkUpSpeed: String;
    public networkDownSpeed: String;
    public wlanSingleIntensity: String;
    public radioSingleIntensity: String;
    public networkDelay_SDK: String;
    public hostFPS: String;
    public deviceFPS: String;
    public deviceDrawCallTime: String;
    public hostDrawCallTimes: String;
    public effectQuality: String;
    public effectQualityMax: String;
    public shadowQuality: String;
    public shadowQualityMax: String;
    public grass: String;
    public grassMax: String;
    public glow: String;
    public glowMax: String;
    public bloom: String;
    public bloomMax: String;
    public msaa: String;
    public msaaMax: String;
    public gpuss_0_stepTemp: String;
    public gpuss_1_stepTemp: String;
    public cpu_0_0_stepTemp: String;
    public cpu_0_1_stepTemp: String;
    public cpu_0_2_stepTemp: String;
    public cpu_0_3_stepTemp: String;
    public cpu_1_0_stepTemp: String;
    public cpu_1_1_stepTemp: String;
    public cpu_1_2_stepTemp: String;
    public cpu_1_3_stepTemp: String;
    public cpu_1_4_stepTemp: String;
    public cpu_1_5_stepTemp: String;
    public cpu_1_6_stepTemp: String;
    public cpu_1_7_stepTemp: String;
    constructor(
        timestamp?: String,
        taskId?: String,
        cpu0Freq?: String, cpu1Freq?: String, cpu2Freq?: String, cpu3Freq?: String, cpu4Freq?: String, cpu5Freq?: String, cpu6Freq?: String, cpu7Freq?: String,
        cpu8Freq?: String, cpu9Freq?: String, cpu10Freq?: String, cpu11Freq?: String,
        cpu0Load?: string, cpu1Load?: string, cpu2Load?: string, cpu3Load?: string, cpu4Load?: string, cpu5Load?: string, cpu6Load?: string, cpu7Load?: string,
        cpu8Load?: string, cpu9Load?: string, cpu10Load?: string, cpu11Load?: string,
        gpuFreq?: String, gpuLoad?: String,
        ddrFreq?: String,
        shellFrame?: String, shellFront?: String, shellBack?: String, socThermal?: String, systemH?: String, gpu?: String, ambient?: String, battery?: String,
        currentNow?: String, voltageNow?: String,
        pss?: String,
        fps?: String,
        fpsJitters?: String,
        networkUpSpeed?: String, networkDownSpeed?: String, cpu1ClusterFre?: String, cpu2ClusterFre?: String, cpu3ClusterFre?: String
    ) {
        this.timestamp = timestamp
        this.taskId = taskId
        this.ambientTemp = ambient
        this.shellBackTemp = shellBack
        this.socThermalTemp = socThermal
        this.batteryTemp = battery
        this.gpuTemp = gpu
        this.shellFrontTemp = shellFront
        this.shellFrameTemp = shellFrame
        this.systemHTemp = systemH
        this.ddrFrequency = ddrFreq
        this.fps = fps
        this.fpsJitters = fpsJitters
        //    this.networkUpSpeed = networkUpSpeed
        this.jank = ""
        this.flag = ""
        this.caton = 0
        this.generalCatonNum = 0
        this.criticalCatonNum = 0
        this.fatalCatonNum = 0
        this.cpu0Frequency = cpu0Freq
        this.cpu1Frequency = cpu1Freq
        this.cpu2Frequency = cpu2Freq
        this.cpu3Frequency = cpu3Freq
        this.cpu4Frequency = cpu4Freq
        this.cpu5Frequency = cpu5Freq
        this.cpu6Frequency = cpu6Freq
        this.cpu7Frequency = cpu7Freq
        this.cpu8Frequency = cpu8Freq
        this.cpu9Frequency = cpu9Freq
        this.cpu10Frequency = cpu10Freq
        this.cpu11Frequency = cpu11Freq
        this.cpu0Load = cpu0Load
        this.cpu1Load = cpu1Load
        this.cpu2Load = cpu2Load
        this.cpu3Load = cpu3Load
        this.cpu4Load = cpu4Load
        this.cpu5Load = cpu5Load
        this.cpu6Load = cpu6Load
        this.cpu7Load = cpu7Load
        this.cpu8Load = cpu8Load
        this.cpu9Load = cpu9Load
        this.cpu10Load = cpu10Load
        this.cpu11Load = cpu11Load
        this.cpuLoad = ""
        this.gpuLoad = gpuLoad
        this.gpuFrequency = gpuFreq
        this.enableHiz = ""
        this.voltageNow = voltageNow
        this.currentNow = currentNow
        this.capacity = ""
        this.status = ""
        this.pss = pss

        this.cpu1ClusterFre = ""
        this.cpu2ClusterFre = ""
        this.cpu3ClusterFre = ""
        //HWCPipeGPU
        this.cacheMisses = ""
        this.instructions = ""
        //HWCPipeGPU
        this.gpuCycles = ""
        this.vertexComputeCycles = ""
        this.fragmentCycles = ""
        this.tilerCycles = ""
        this.vertexComputeJobs = ""
        this.fragmentJobs = ""
        this.pixels = ""
        this.earlyZTests = ""
        this.earlyZKilled = ""
        this.externalMemoryReadAccesses = ""
        this.externalMemoryWriteAccesses = ""
        this.externalMemoryReadBytes = ""
        this.externalMemoryWriteBytes = ""
        this.cacheWriteLookups = ""
        this.cacheReadLookups = ""
        this.externalMemoryWriteStalls = ""
        this.externalMemoryReadStalls = ""
        this.shaderCycles = ""
        this.shaderArithmeticCycles = ""
        this.shaderLoadStoreCycles = ""
        this.shaderTextureCycles = ""
        //QPCounters
        this.clocksSecond = ""
        this.gpuUtilization = ""
        this.gpuBusBusy = ""
        this.verticesShadedSecond = ""
        this.fragmentsShadedSecond = ""
        this.texturesVertex = ""
        this.texturesFragment = ""
        this.aluVertex = ""
        this.aluFragment = ""
        this.timeShadingFragments = ""
        this.timeShadingVertices = ""
        this.timeCompute = ""
        this.readTotal = ""
        this.writeTotal = ""
        this.textureMemoryReadBW = ""
        this.vertexMemoryRead = ""
        this.spMemoryRead = ""
        this.qpGPUFrequency = ""
        //network
        this.currNetworkType = ""
        this.networkUpSpeed = networkUpSpeed
        this.networkDownSpeed = networkDownSpeed
        this.wlanSingleIntensity = ""
        this.radioSingleIntensity = ""
        this.networkDelay_SDK = ""
        this.hostFPS = ""
        this.deviceFPS = ""
        this.deviceDrawCallTime = ""
        this.hostDrawCallTimes = ""
        this.effectQuality = ""
        this.effectQualityMax = ""
        this.shadowQuality = ""
        this.shadowQualityMax = ""
        this.grass = ""
        this.grassMax = ""
        this.bloom = ""
        this.bloomMax = ""
        this.msaa = ""
        this.msaaMax = ""
        this.glow = ''
        this.glowMax = ''
        this.gpuss_0_stepTemp = ""
        this.gpuss_1_stepTemp = ""
        this.cpu_0_0_stepTemp = ""
        this.cpu_0_1_stepTemp = ""
        this.cpu_0_2_stepTemp = ""
        this.cpu_0_3_stepTemp = ""
        this.cpu_1_0_stepTemp = ""
        this.cpu_1_1_stepTemp = ""
        this.cpu_1_2_stepTemp = ""
        this.cpu_1_3_stepTemp = ""
        this.cpu_1_4_stepTemp = ""
        this.cpu_1_5_stepTemp = ""
        this.cpu_1_6_stepTemp = ""
        this.cpu_1_7_stepTemp = ""
        this.cpu1ClusterFre = cpu1ClusterFre
        this.cpu2ClusterFre = cpu2ClusterFre
        this.cpu3ClusterFre = cpu3ClusterFre
    }

    setTimeStamp(timestamp: String) {
        this.timestamp = timestamp
    }

    setTaskId(taskId: String) {
        this.taskId = taskId
    }

    setCPUData(cpu0Freq: String, cpu1Freq: String, cpu2Freq: String, cpu3Freq: String, cpu4Freq: String, cpu5Freq: String, cpu6Freq: String, cpu7Freq: String,
               cpu8Freq: String, cpu9Freq: String, cpu10Freq: String, cpu11Freq: String, cpuCoreNum: number) {
        this.cpu0Frequency = cpu0Freq
        this.cpu1Frequency = cpu1Freq
        this.cpu2Frequency = cpu2Freq
        this.cpu3Frequency = cpu3Freq
        this.cpu4Frequency = cpu4Freq
        this.cpu5Frequency = cpu5Freq
        this.cpu6Frequency = cpu6Freq
        this.cpu7Frequency = cpu7Freq
        this.cpu8Frequency = cpu8Freq
        this.cpu9Frequency = cpu9Freq
        this.cpu10Frequency = cpu10Freq
        this.cpu11Frequency = cpu11Freq
        if (cpuCoreNum == 8) {
            this.cpu1ClusterFre = cpu0Freq
            this.cpu2ClusterFre = cpu4Freq
            this.cpu3ClusterFre = cpu7Freq
        }else{
            this.cpu1ClusterFre = cpu0Freq
            this.cpu2ClusterFre = cpu4Freq
            this.cpu3ClusterFre = cpu10Freq
        }
    }

    setCPULoadData(cpu0Load: String, cpu1Load: String, cpu2Load: String, cpu3Load: String, cpu4Load: String, cpu5Load: String, cpu6Load: String, cpu7Load: String,
                   cpu8Load: String, cpu9Load: String, cpu10Load: String, cpu11Load: String, cpuCoreNum: number) {
        this.cpu0Load = cpu0Load
        this.cpu1Load = cpu1Load
        this.cpu2Load = cpu2Load
        this.cpu3Load = cpu3Load
        this.cpu4Load = cpu4Load
        this.cpu5Load = cpu5Load
        this.cpu6Load = cpu6Load
        this.cpu7Load = cpu7Load
        this.cpu8Load = cpu8Load
        this.cpu9Load = cpu9Load
        this.cpu10Load = cpu10Load
        this.cpu11Load = cpu11Load
        if (cpuCoreNum == 8) {
            this.cpuLoad = ((Number(cpu0Load) + Number(cpu1Load) + Number(cpu2Load) + Number(cpu3Load) + Number(cpu4Load) + Number(cpu5Load) + Number(cpu6Load) + Number(cpu7Load)) / 8 ).toString()
        }else{
            this.cpuLoad = ((Number(cpu0Load) + Number(cpu1Load) + Number(cpu2Load) + Number(cpu3Load) + Number(cpu4Load) + Number(cpu5Load) + Number(cpu6Load) + Number(cpu7Load)
            + Number(cpu8Load) + Number(cpu9Load) + Number(cpu10Load) + Number(cpu11Load)
            ) / 12 ).toString()
        }
    }

    setGPUData(gpuFreq: String, gpuLoad: String) {
        this.gpuFrequency = gpuFreq
        this.gpuLoad = gpuLoad
    }

    setDDRData(ddrFreq: String) {
        this.ddrFrequency = ddrFreq
    }

    setTempData(shellFrame: String, shellFront: String, shellBack: String, soc_thermal: String, system_h: String, gpu: String, Battery: String) {
        this.shellFrameTemp = (Number(shellFrame) * 0.001).toString()
        this.shellFrontTemp = (Number(shellFront) * 0.001).toString()
        this.shellBackTemp = (Number(shellBack) * 0.001).toString()
        this.socThermalTemp = (Number(soc_thermal) * 0.001).toString()
        this.systemHTemp = (Number(system_h) * 0.001).toString()
        this.gpuTemp = gpu
        this.batteryTemp = (Number(Battery) * 0.001).toString()

    }

    setPowerData(currentNow: String, voltageNow: String, capacity:String, status: String) {
        this.currentNow = currentNow
        this.voltageNow = voltageNow
        this.capacity = capacity
        this.status = status
    }

    setFpsData(fps: String, fpsJitter: String) {
        this.fps = fps
        this.fpsJitters = fpsJitter
    }

    setRamData(pss: String) {
        this.pss = pss
    }

    setNetWorkData(networkUpSpeed:String, networkDownSpeed:String){
        this.networkDownSpeed = networkDownSpeed
        this.networkUpSpeed = networkUpSpeed
    }

    setDefaultValue() {
        let properties: any[] = Object.keys(this)
        properties.forEach(property => {
            this[property] = "0"
        })
    }
}

//dubai app功耗
export class TPowerAppInfo {
    //主键 自增
    public id: String;
    public taskId: String;
    public application: String;
    public power: String;
    public current: String;
    public percent: String;
    public color: Resource;
    constructor(id ?: String, taskId ?: String, application ?: String, power ?: String, current ?: String, percent ?: String,color?: Resource) {
        this.id = id
        this.taskId = taskId
        this.application = application
        this.power = power
        this.current = current
        this.percent = percent
        this.color = color
    }
    setColor(color?: Resource){
        this.color = color
    }
    setPower(power?: String){
        this.power = power
    }
    setPercent(percent?: String){
        this.percent = percent
    }
    setCurrent(current?: String){
        this.current = current
    }
}

//dubai 器件功耗
export class TPowerSensorInfo {
    //主键 自增
    public id: string;
    public taskId: string;
    public sensor: string;
    public power: string;
    public current: string;
    public percent: string;
    public color: Resource;
    constructor( taskId ?: string, sensor ?:string, power ?: string, current ?: string, percent ?: string,color?: Resource) {
        this.taskId = taskId
        this.sensor = sensor
        this.power = power
        this.current = current
        this.percent = percent
        this.color = color
    }
    setPerenct(percent: string) {
        this.percent = percent
    }
    setColor(color: Resource) {
        this.color = color
    }
    setPower(power: string) {
        this.power = power
    }
    setCurrent(current: string) {
        this.current = current
    }
}

// sceneInfo
export class SceneInfo {
    // 场景时长(column = 单位毫秒)
    private useTime: String
    // 场景id
    private sceneId: String
    private taskId: String
    // 场景开始时间戳
    private startTime: String
    // 场景结束时间戳
    private endTime: String
    // 场景名称
    private sceneName: String
    // 满帧
    private fps: String
    // 平均帧率
    private averageFrameRate: String
    // 帧率中位数
    private medianFrameRate: String
    // 抖动率
    private jitterRate: String
    // 最差丢帧
    private minFrame: String
    // 最差丢帧
    private worstDropFPS: String
    // 低帧率
    private lowFrameRate: String
    private initShellTemp: String
    private maxShellTemp: String
    private shellTemRise: String
    private maxSystemTem: String
    private systemTemRise: String
    private charging: String
    private enduranceTimes: String
    private electricity: String
    private fpsEnergy: String
    private avgNetworkDelay: String
    private hightDelayRate: String
    private lowFrameRateScore: String
    private jitterRateScore: String
    private generalCationRate: String
    private criticalCatonRate: String
    private fatalCatonRate: String
    private cpuC1Frequency: String
    private cpuC2Frequency: String
    private cpuC3Frequency: String
    private gpuFrequency: String
    private ddrFrequency: String
    private catonRate: String
    private averageCurrentNow: String
    private averageNetworkDelay: String
    private maxNetworkDelay: String
    private highNetworkDelay: String
    private superHighNetworkDelay: String
    constructor ( useTime?: String, sceneId?: String, taskId?: String, startTime?: String, endTime?: String, sceneName?: String, fps?: String, averageFrameRate?: String,
                  medianFrameRate?: String, jitterRate?: String, minFrame?: String, worstDropFPS?: String, lowFrameRate?: String, initShellTemp?: String, maxShellTemp?: String,
                  shellTemRise?: String, maxSystemTem?: String, systemTemRise?: String, charging?: String, enduranceTimes?: String, electricity?: String, fpsEnergy?: String,
                  avgNetworkDelay?: String, hightDelayRate ?: String, lowFrameRateScore?: String, jitterRateScore?: String, generalCationRate?: String, criticalCatonRate?: String, fatalCatonRate?: String,
                  cpuC1Frequency?: String, cpuC2Frequency?: String, cpuC3Frequency?: String,
                  gpuFrequency?: String, ddrFrequency?: String, catonRate?: String, averageCurrentNow?: String, averageNetworkDelay?: String, maxNetworkDelay?: String,
                  highNetworkDelay?: String, superHighNetworkDelay?: String) {
        this.useTime = useTime
        this.sceneId = sceneId
        this.taskId = taskId
        this.startTime = startTime
        this.endTime = endTime
        this.sceneName = sceneName
        this.fps = fps
        this.averageFrameRate = averageFrameRate
        this.medianFrameRate = medianFrameRate
        this.jitterRate = jitterRate
        this.minFrame = minFrame
        this.worstDropFPS = worstDropFPS
        this.lowFrameRate = lowFrameRate
        this.initShellTemp = initShellTemp
        this.maxShellTemp = maxShellTemp
        this.shellTemRise = shellTemRise
        this.maxSystemTem = maxSystemTem
        this.systemTemRise = systemTemRise
        this.charging = charging
        this.enduranceTimes = enduranceTimes
        this.electricity = electricity
        this.fpsEnergy = fpsEnergy
        this.avgNetworkDelay = avgNetworkDelay
        this.hightDelayRate = hightDelayRate
        this.lowFrameRateScore = lowFrameRateScore
        this.jitterRateScore = jitterRateScore
        this.generalCationRate = generalCationRate
        this.criticalCatonRate = criticalCatonRate
        this.fatalCatonRate = fatalCatonRate
        this.cpuC1Frequency = cpuC1Frequency
        this.cpuC2Frequency = cpuC2Frequency
        this.cpuC3Frequency = cpuC3Frequency
        this.gpuFrequency = gpuFrequency
        this.ddrFrequency = ddrFrequency
        this.catonRate = catonRate
        this.averageCurrentNow = averageCurrentNow
        this.averageNetworkDelay = averageNetworkDelay
        this.maxNetworkDelay = maxNetworkDelay
        this.highNetworkDelay = highNetworkDelay
        this.superHighNetworkDelay = superHighNetworkDelay
    }
    setDefaultValue() {
        let properties: any [] = Object.keys(this)
        properties.forEach(property => {
            this[property]= "0"
        })
    }
    setUseTime(useTime: string) {
        this.useTime = useTime
        return this
    }
    setSceneId(sceneId: string) {
        this.sceneId = sceneId
        return this
    }
    setTaskId(taskId: string) {
        this.taskId = taskId
        return this
    }
    setStartTime(startTime: string) {
        this.startTime = startTime
        return this
    }
    setEndTime(endTime: string) {
        this.endTime = endTime
        return this
    }
    setSceneName(sceneName: string) {
        this.sceneName = sceneName
        return this
    }
    setFps(fps: string) {
        this.fps = fps
        return this
    }
    setAverageFrameRate(averageFrameRate: string) {
        this.averageFrameRate = averageFrameRate
        return this
    }
    setMedianFrameRate(medianFrameRate: string) {
        this.medianFrameRate = medianFrameRate
        return this
    }
    setJitterRate(jitterRate: string) {
        this.jitterRate = jitterRate
        return this
    }
    setMinFrame(minFrame: string) {
        this.minFrame = minFrame
        return this
    }
    setWorstDropFPS(worstDropFPS: string) {
        this.worstDropFPS = worstDropFPS
        return this
    }
    setLowFrameRate(lowFrameRate: string) {
        this.lowFrameRate = lowFrameRate
        return this
    }
    setInitShellTemp(initShellTemp: string) {
        this.initShellTemp = initShellTemp
        return this
    }
    setMaxShellTemp(maxShellTemp: string) {
        this.maxShellTemp = maxShellTemp
        return this
    }
    setShellTemRise(shellTemRise: string) {
        this.shellTemRise = shellTemRise
        return this
    }
    setMaxSystemTem(maxSystemTem: string) {
        this.maxSystemTem = maxSystemTem
        return this
    }
    setSystemTemRise(systemTemRise: string) {
        this.systemTemRise = systemTemRise
        return this
    }
    setCharging(charging: string) {
        this.charging = charging
        return this
    }
    setEnduranceTimes(enduranceTimes: string) {
        this.enduranceTimes = enduranceTimes
        return this
    }
    setElectricity(electricity: string) {
        this.electricity = electricity
        return this
    }
    setFpsEnergy(fpsEnergy: string) {
        this.fpsEnergy = fpsEnergy
        return this
    }
    setAvgNetworkDelay(avgNetworkDelay: string) {
        this.avgNetworkDelay = avgNetworkDelay
        return this
    }
    setHightDelayRate(hightDelayRate: string) {
        this.hightDelayRate = hightDelayRate
        return this
    }
    setLowFrameRateScore(lowFrameRateScore: string) {
        this.lowFrameRateScore = lowFrameRateScore
        return this
    }
    setJitterRateScore(jitterRateScore: string) {
        this.jitterRateScore = jitterRateScore
        return this
    }
    setGeneralCationRate(generalCationRate: string) {
        this.generalCationRate = generalCationRate
        return this
    }
    setCriticalCatonRate(criticalCatonRate: string) {
        this.criticalCatonRate = criticalCatonRate
        return this
    }
    setFatalCatonRate(fatalCatonRate: string) {
        this.fatalCatonRate = fatalCatonRate
        return this
    }
    setCpuC1Frequency(cpuC1Frequency: string) {
        this.cpuC1Frequency = cpuC1Frequency
        return this
    }
    setCpuC2Frequency(cpuC2Frequency: string) {
        this.cpuC2Frequency = cpuC2Frequency
        return this
    }
    setCpuC3Frequency(cpuC3Frequency: string) {
        this.cpuC3Frequency = cpuC3Frequency
        return this
    }
    setGpuFrequency(gpuFrequency: string) {
        this.gpuFrequency = gpuFrequency
        return this
    }
    setDdrFrequency(ddrFrequency: string) {
        this.ddrFrequency = ddrFrequency
        return this
    }
    setCatonRate(catonRate: string) {
        this.catonRate = catonRate
        return this
    }
    setAverageCurrentNow(averageCurrentNow: string) {
        this.averageCurrentNow = averageCurrentNow
        return this
    }
    setAverageNetworkDelay(averageNetworkDelay: string) {
        this.averageNetworkDelay = averageNetworkDelay
        return this
    }
    setMaxNetworkDelay(maxNetworkDelay: string) {
        this.maxNetworkDelay = maxNetworkDelay
        return this
    }
    setHighNetworkDelay(highNetworkDelay: string) {
        this.highNetworkDelay = highNetworkDelay
        return this
    }
    setSuperHighNetworkDelay(superHighNetworkDelay: string) {
        this.superHighNetworkDelay = superHighNetworkDelay
        return this
    }
    getUseTime() {
        return this.useTime
    }
    getSceneId() {
        return this.sceneId
    }
    getTaskId() {
        return this.taskId
    }
    getStartTime() {
        return this.startTime
    }
    getEndTime() {
        return this.endTime
    }
    getSceneName() {
        return this.sceneName
    }
    getFps() {
        return this.fps
    }
    getAverageFrameRate() {
        return this.averageFrameRate
    }
    getMedianFrameRate() {
        return this.medianFrameRate
    }
    getJitterRate() {
        return this.jitterRate
    }
    getMinFrame() {
        return this.minFrame
    }
    getWorstDropFPS() {
        return this.worstDropFPS
    }
    getLowFrameRate() {
        return this.lowFrameRate
    }
    getInitShellTemp() {
        return this.initShellTemp
    }
    getMaxShellTemp() {
        return this.maxShellTemp
    }
    getShellTemRise() {
        return this.shellTemRise
    }
    getMaxSystemTem() {
        return this.maxSystemTem
    }
    getSystemTemRise() {
        return this.systemTemRise
    }
    getCharging() {
        return this.charging
    }
    getEnduranceTimes() {
        return this.enduranceTimes
    }
    getElectricity() {
        return this.electricity
    }
    getFpsEnergy() {
        return this.fpsEnergy
    }
    getAvgNetworkDelay() {
        return this.avgNetworkDelay
    }
    getHightDelayRate() {
        return this.hightDelayRate
    }
    getLowFrameRateScore() {
        return this.lowFrameRateScore
    }
    getJitterRateScore() {
        return this.jitterRateScore
    }
    getGeneralCationRate() {
        return this.generalCationRate
    }
    getCriticalCatonRate() {
        return this.criticalCatonRate
    }
    getFatalCatonRate() {
        return this.fatalCatonRate
    }
    getCpuC1Frequency(){
        return this.cpuC1Frequency
    }
    getCpuC2Frequency(){
        return this.cpuC2Frequency
    }
    getCpuC3Frequency(){
        return this.cpuC3Frequency
    }
    getGpuFrequency() {
        return this.gpuFrequency
    }
    getDdrFrequency() {
        return this.ddrFrequency
    }
    getCatonRate() {
        return this.catonRate
    }
    getAverageCurrentNow() {
        return this.averageCurrentNow
    }
    getAverageNetworkDelay() {
        return this.averageNetworkDelay
    }
    getMaxNetworkDelay() {
        return this.maxNetworkDelay
    }
    getHighNetworkDelay() {
        return this.highNetworkDelay
    }
    getSuperHighNetworkDelay() {
        return this.superHighNetworkDelay
    }
}

// performInfo
export class PerformInfo {
    public id: String;
    public name: String;
    public type: String;
    public package: String;
    public value: String;
    public startTime: String;
    public endTime: String;
    constructor(id?, name?, type?, packageName?, value?, startTime?, endTime?) {
        this.id = id
        this.name = name
        this.type = type
        this.package = packageName
        this.value = value
        this.startTime = startTime
        this.endTime = endTime
    }
}

// 帧率测试信息
export class FrameRateInfo {
    public id: String
    public name: String
    public type: String
    public package: String
    public value: String
    public startTime: String
    public endTime: String
    constructor(id?, name?, type?, packageName?, value?, startTime?, endTime?) {
        this.id = id
        this.name = name
        this.type = type
        this.package = packageName
        this.value = value
        this.startTime = startTime
        this.endTime = endTime
    }
}

