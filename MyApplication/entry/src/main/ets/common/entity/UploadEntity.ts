/**
* 创建任务请求体
* @param jobId
* @param jobName
* @param createTimestamp
* @param startTimestamp
* @param endTimestamp
* @param deviceSn
* @param deviceName
* @param expand
* @param deviceType
* @param deviceTypeName
* @param deviceVersion
* @param jobType
* @param subJobType
* @param subJobTypeName
* @param jobState
* @param progress
*/
export class CreateTaskBody {
    private jobId: String
    private jobName: String
    private jobType: String
    private subJobType: String
    private subJobTypeName: String
    private jobState: String
    private progress: String
    private createTimestamp: Number
    private startTimestamp: Number
    private endTimestamp: Number
    private deviceSn: String
    private deviceName: String
    private deviceType: String
    private deviceTypeName: String
    private deviceVersion: String
    private expand: Expand
    constructor(jobId: String="",
                jobName?: String,
                createTimestamp?: Number,
                startTimestamp?: Number,
                endTimestamp?: Number,
                deviceSn?: String,
                deviceName?: String,
                expand?: Expand,
                deviceType: String="default",
                deviceTypeName: String="手机",
                deviceVersion: String="",
                jobType: String="图形图像测试",
                subJobType: String="game_smartperf",
                subJobTypeName: String="游戏测试-OH-SmartPerf",
                jobState: String="running",
                progress: String="0"
    ){
        this.jobId = jobId
        this.jobName = jobName
        this.jobType = jobType
        this.subJobType = subJobType
        this.subJobTypeName = subJobTypeName
        this.jobState = jobState
        this.progress = progress
        this.createTimestamp = createTimestamp
        this.startTimestamp = startTimestamp
        this.endTimestamp = endTimestamp
        this.deviceSn = deviceSn
        this.deviceName = deviceName
        this.deviceType = deviceType
        this.deviceTypeName = deviceTypeName
        this.deviceVersion = deviceVersion
        this.expand = expand
    }
    getJobName() {
        return this.jobName
    }
    getExpand() {
            return this.expand
    }
    getStartTimestamp() {
            return this.startTimestamp
    }
    setJobId(jobId: String) {
            this.jobId = jobId
            return this
    }
    setJobName(jobName: String) {
            this.jobName = jobName
            return this
    }
    setCreateTimestamp(createTimestamp: Number) {
            this.createTimestamp = createTimestamp
            return this
    }
    setStartTimestamp(startTimestamp: Number) {
            this.startTimestamp = startTimestamp
            return this
    }
    setEndTimestamp(endTimestamp: Number) {
            this.endTimestamp = endTimestamp
            return this
    }
    setDeviceSn(deviceSn: String) {
            this.deviceSn = deviceSn
            return this
    }
    setDeviceName(deviceName: String) {
            this.deviceName = deviceName
            return this
    }
    setExpand(expand: Expand) {
            this.expand = expand
            return this
    }
    setDeviceType(deviceType: String) {
            this.deviceType = deviceType
            return this
    }
    setDeviceTypeName(deviceTypeName: String) {
            this.deviceTypeName = deviceTypeName
            return this
    }
    setDeviceVersion(deviceVersion: String) {
            this.deviceVersion = deviceVersion
            return this
    }
    setJobType(jobType: String) {
            this.jobType = jobType
            return this
    }
    setSubJobType(subJobType: String) {
            this.subJobType = subJobType
            return this
    }
    setSubJobTypeName(subJobTypeName: String){
            this.subJobTypeName = subJobTypeName
            return this
    }
    setJobState(jobState: String) {
            this.jobState = jobState
            return this
    }
    setProgress(progress: String) {
            this.progress = progress
            return this
    }
    setDefaultData() {
        this.deviceType = "default",
        this.deviceTypeName = "手机",
        this.deviceVersion = "",
        this.jobType="图形图像测试",
        this.subJobType="game_smartperf",
        this.subJobTypeName="游戏测试-SmartPerf",
        this.jobState="running",
        this.progress="0"
        return this
    }
    build() {
        return this
    }
}

export class Expand {
    private userName: String
    private appName: String
    private board: String
    private sn: String
    private appInfo: appInfo
    constructor(userName: String, appName: String, board: String, sn: String, appInfo: appInfo) {
        this.userName = userName
        this.appName = appName
        this.board = board
        this.sn = sn
        this.appInfo = appInfo
    }

    getAppInfo(){
        return this.appInfo
    }

}

export class appInfo {
    private appVersion: String
    private appName: String
    private packageName: String
    constructor(appVersion: String, appName: String, packageName: String) {
        this.appName = appName
        this.appVersion = appVersion
        this.packageName = packageName
    }
    getAppVersion() {
        return this.appVersion
    }
    getAppName() {
        return this.appName
    }
    getPackageName() {
        return this.packageName
    }
}

export class TrackerBody {
    // 打点事件Id 截止至2021 6.29有    806001001登录Id, 806001002创建任务
    private eventId: String
    // 用户I=id
    private userId: String
    // 发生时间
    private happenTime: String
    // 额外的数据
    private serviceName: String
    private extraData: ExtraData
    constructor(eventId?: String, userId?: String, happenTime?: String, serviceName?: String, extraData?: ExtraData) {
        this.eventId = eventId
        this.userId = userId
        this.happenTime = happenTime
        this.serviceName = serviceName
        this.extraData = extraData
    }
    setData(eventId?: String, userId?: String, happenTime?: String, serviceName?: String){
        this.eventId = eventId
        this.userId = userId
        this.happenTime = happenTime
        this.serviceName = serviceName
    }
    setEventId(eventId: String) {
        this.eventId = eventId
        return this
    }
    setUserId(userId: String) {
        this.userId = userId
        return this
    }
    setHappenTime(happenTime: String) {
        this.happenTime = happenTime
        return this
    }
    setServiceName(serviceName: String) {
        this.serviceName = serviceName
        return this
    }
    setExtraData(extraData ?: ExtraData) {
        this.extraData = extraData
        return this
    }
    build() {
        return this
    }
}

export class ExtraData {
    // input (登录操作) / auto (已登录过)
    private loginType: String
    private projectId: String
    private loginTime: String
    private jobName: String
    private brand: String
    private board: String
    private version: String
    private appName: String
    private appPackageName: String
    private createTime: String
    private sn: String
    private licenseHashIds: Array<String> // 检查的license项id 字符串数组
    private checkResult: String //检查成功或失败
    private failedContent: String //失败原因简述
    constructor(loginType?: String, projectId?: String, loginTime?: String, jobName?: String, brand?: String, board?: String, version?: String, appName?: String,
                appPackageName?: String, createTime?: String, sn?: String, licenseHashIds?: Array<String>, checkResult?: String, failedContent?: String){
        this.loginType = loginType
        this.projectId = projectId
        this.loginTime = loginTime
        this.jobName = jobName
        this.brand = brand
        this.board = board
        this.version = version
        this.appName = appName
        this.appPackageName = appPackageName
        this.createTime = createTime
        this.sn = sn
        this.licenseHashIds = licenseHashIds
        this.checkResult = checkResult
        this.failedContent = failedContent
    }
    setProjectId(projectId: String) {
        this.projectId = projectId
        return this
    }
    setLoginType(loginType: String) {
        this.loginType = loginType
        return this
    }
    setLoginTime(loginTime: String) {
        this.loginTime = loginTime
        return this
    }
    setJobName(jobName: String) {
        this.jobName = jobName
        return this
    }
    setBrand(brand: String) {
        this.brand = brand
        return this
    }
    setBoard(board: String) {
        this.board = board
        return this
    }
    setVersion(version: String) {
        this.version = version
        return this
    }
    setAppName(appName: String) {
        this.appName = appName
        return this
    }
    setAppPackageName(appPackageName: String) {
        this.appPackageName = appPackageName
        return this
    }
    setCreateTime(createTime: String) {
        this.createTime = createTime
        return this
    }
    setSn(sn: String) {
        this.sn = sn
        return this
    }
    setLicenseHashIds(licenseHashIds: Array<String>) {
        this.licenseHashIds = licenseHashIds
        return this
    }
    setCheckResult(checkResult: String) {
        this.checkResult = checkResult
        return this
    }
    setFailedContent(failedContent: String) {
        this.failedContent = failedContent
        return this
    }
    build() {
        return this
    }
}

export class UploadSuccessBody{
    jobId: String
    jobName: String
    jobType: String
    subJobType: String
    createTimestamp: number
    startTimestamp: number
    endTimestamp: number
    jobState: String
    userId: String
    jobResult: JobResult
    taskDetail: Array<TaskDetail>
    constructor(jobId?: String, jobName?: Sting, jobType?: String, subJobType?: String, createTimestamp?: number, startTimestamp?: number, endTimestamp?: number,
                jobState?: String, userId?: String, jobResult?: JobResult, taskDetail?: Array<TaskDetail>) {
        this.jobId = jobId
        this.jobName = jobName
        this.jobType = jobType
        this.subJobType = subJobType
        this.createTimestamp = createTimestamp
        this.startTimestamp = startTimestamp
        this.endTimestamp = endTimestamp
        this.jobState = jobState
        this.userId = userId
        this.jobResult = jobResult
        this.taskDetail = taskDetail
    }
    setJobId(jobId: String) {
        this.jobId = jobId
        return this
    }
    setJobName(jobName: String) {
        this.jobName = jobName
        return this
    }
    setJobType(jobType: String) {
        this.jobType = jobType
        return this
    }
    setSubJobType(subJobType: String) {
        this.subJobType = subJobType
        return this
    }
    setCreateTimestamp(createTimestamp: Number) {
        this.createTimestamp = createTimestamp
        return this
    }
    setStartTimestamp(startTimestamp: Number) {
        this.startTimestamp = startTimestamp
        return this
    }
    setEndTimestamp(endTimestamp: Number) {
        this.endTimestamp = endTimestamp
        return this
    }
    setJobState(jobState: String) {
        this.jobState = jobState
        return this
    }
    setUserId(userId: String) {
        this.userId = userId
        return this
    }
    setJobResult(jobResult: JobResult) {
        this.jobResult = jobResult
        return this
    }
    setTaskDetail(taskDetail: Array<TaskDetail>) {
        this.taskDetail = taskDetail
        return this
    }
}

export class JobResult {
    total?: number
    pass?: number
    constructor(total?: number, pass?: number) {
        this.total = total
        this.pass = pass
    }
},

export class TimeDetail {
    startTimestamp? :number
    endTimestamp?: number
    constructor(startTimestamp? :number, endTimestamp?: number) {
        this.startTimestamp = startTimestamp
        this.endTimestamp = endTimestamp
    }
},

export class TaskExpand {
    zipUrl?: String,
    fileName?: String,
    beta: Boolean,
    testType?: String,
    constructor (
        zipUrl?: String,
        fileName?: String
        beta?: Boolean
        testType?: String
    ) {
    this.zipUrl = zipUrl
    this.fileName = fileName
    this.beta = beta
    this.testType = testType
    }
}

export class TaskDetail {
    testTaskId?: String
    taskId?: String
    deviceName?: String
    deviceSn?: String
    deviceVersion?: String
    timeDetail?: TimeDetail
    taskExpand?: TaskExpand
    constructor(
        testTaskId?: String,
        taskId?: String,
        deviceName?: String,
        deviceSn?: String,
        deviceVersion?: String,
        timeDetail?: TimeDetail,
        taskExpand?: TaskExpand
    ) {
        this.testTaskId = testTaskId
        this.taskId = taskId
        this.deviceName = deviceName
        this.deviceSn = deviceSn
        this.deviceVersion = deviceVersion
        this.timeDetail = timeDetail
        this.taskExpand = taskExpand
    }
}

export class FileBuffer {
    buffer: ArrayBuffer
    length: number
    constructor(buffer: ArrayBuffer, length: number) {
        this.buffer = buffer
        this.length = length
    }
    get Buffer() {
        return this.buffer
    }
    get Length() {
        return this.length
    }
    setBuffer(buffer: ArrayBuffer) {
        this.buffer = buffer
    }
}