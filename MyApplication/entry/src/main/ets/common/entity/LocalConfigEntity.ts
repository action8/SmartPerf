/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export enum TestMode {
    APPTEST,
    BRIGHTNESS,
    STARTUP,
    SWIPE,
}

export class OtherSupport {
    public testName: string
    public testSrc: string
    public testMode: TestMode
    public resource: Resource

    constructor(testName: string, testSrc: string, testMode: TestMode, resource: Resource) {
        this.testName = testName
        this.testSrc = testSrc
        this.testMode = testMode
        this.resource = resource
    }
}

export class SwitchItem {
    public id: string
    public switchName: string
    public switchSrc: Resource
    public isOpen: boolean
    public enable: boolean

    constructor(id: string, switchName: string, switchSrc: Resource, isOpen: boolean, enable: boolean) {
        this.id = id
        this.switchName = switchName
        this.switchSrc = switchSrc
        this.isOpen = isOpen
        this.enable = enable
    }
}

export enum TraceConfigType {
    TRACE_SUM,
    LOST_FPS,
    FPS_JITTER_TIME,
    CATCH_INTERVAL
}

export class TraceConfig {
    public traceSum?: number = 2
    public lowFps?: number = 20
    public fpsJitterTime?: number = 133
    public catchInterval?: number = 60
    public traceCmd: string
    constructor(traceSum?: number, lowFps?: number, fpsJitterTime?: number, catchInterval?: number, traceCmd?: string) {
        this.traceSum = traceSum
        this.lowFps = lowFps
        this.fpsJitterTime = fpsJitterTime
        this.catchInterval = catchInterval
        this.traceCmd = traceCmd
    }
}

export class CollectItem {
    public name: string
    public isSupport: boolean
    public isSelect: boolean

    constructor(name: string, isSupport: boolean, isSelect: boolean) {
        this.name = name
        this.isSupport = isSupport
        this.isSelect = isSelect
    }
}

export class ArticleItem {
    public name: string
    public value: string
    constructor(name: string, value: string) {
        this.name = name
        this.value = value
    }
}

export class PerformanceItem {
    public name: string
    public isSupport: boolean
    public isSelect: boolean

    constructor(name: string, isSupport: boolean, isSelect: boolean) {
        this.name = name
        this.isSupport = isSupport
        this.isSelect = isSelect
    }
}

export class TaskInfoConfig {
    public testName: string
    public collectItem: Array<CollectItem>
    public switchItem: Array<SwitchItem>

    constructor(testName?: string, collectItem?: Array<CollectItem>, switchItem?: Array<SwitchItem>) {
        this.testName = testName
        this.collectItem = collectItem
        this.switchItem = switchItem
    }
}

export class AppInfoItem {
    public id: number
    public packageName: string
    public appName: string
    public appVersion: String
    public appIcon: string
    public abilityName: string

    constructor(packageName?: string, appName?: string, appVersion?: String, appIcon?: string, abilityName?: string, iconId?: number) {
        this.packageName = packageName
        this.appName = appName
        this.appVersion = appVersion
        this.appIcon = appIcon
        this.abilityName = abilityName
        this.id = iconId
    }
}

export class ReportItem {
    public sessionId: String;
    public taskId: String;
    public dbPath: String;
    public packageName: String;
    public iconId: String;
    public name: String;
    public appName: String;
    public startTime: String;
    public testDuration: String;
    public upStatus: String;
    public projectId: String;

    constructor(sessionId: String, taskId: String, dbPath: String, packageName: String, iconId: String, name: String,
                appName: String, startTime: String, testDuration: String, upStatus: String, projectId: String) {
        this.sessionId = sessionId
        this.taskId = taskId
        this.dbPath = dbPath
        this.packageName = packageName
        this.iconId = iconId
        this.name = name
        this.appName = appName
        this.startTime = startTime
        this.testDuration = testDuration
        this.upStatus = upStatus
        this.projectId = projectId
    }

    public getStartTime(): string{
        return this.startTime.valueOf()
    }

    public getTestDuration(): string{
        return this.testDuration.valueOf()
    }

    public getDbPath(): string{
        return this.dbPath.valueOf()
    }
}



export class QuestionItem {
    public question: string
    public answer: string

    constructor(question: string, answer: string) {
        this.answer = answer
        this.question = question
    }
}

export const questionList = new Array(
    new QuestionItem('1.欢迎交流：', 'Welink群号：592130390957921111'),
    new QuestionItem('2.SmartPerf(Device)简介', 'SmartPerf是基于OpenHarmony系统开发的性能功耗测试工具，操作简单易用，可提供包括性能、功耗的关键KPI指标，给出具体指标的测试值、包括采集设备的FPS、CPU、GPU指标等数据。'),
    new QuestionItem('3.SP工具使用注意事项：', '新版本支持云端数据展示功能，登录测试时需要连接网络，登录时账号：工号，邮箱：华为邮箱。'),
    new QuestionItem('4.SmartPerf测试操作步骤', '首页-开始测试-勾选采集项-设置任务名-开始测试-单击悬浮窗开始任务(双击悬浮窗打开详情页)-长按结束'),
    new QuestionItem('5.SmartPerf测试报告查看', '首页-报告-点击报告-滑动查看。'),
    new QuestionItem('6.无法勾选FPS和RAM采集', '需要开启SP_daemon,连接usb线执行 "hdc_std shell SP_daemon" 激活即可。'),
    new QuestionItem('7.SP测试数据上传操作', '首页-报告页-点击上传按钮-确认上传-切换刷新页面-显示已上传 上传报告查看地址：https://devecotesting.rnd.huawei.com/micro/manageCenter/cloud/game/。'),
    new QuestionItem('8.SP后续规划?', '集成更多采集能力,如启动时延、滑动帧率、trace采集,gpu counter采集,网络采集等等;优化数据展示方式,报告上传网站端,在线分析性能功耗问题。')
)

export class SummaryItem {
    public icon: Resource
    public content: string
    public value: string
    public backColor: string

    constructor(icon: Resource, content: string, value: string, backColor: string) {
        this.icon = icon
        this.content = content
        this.value = value
        this.backColor = backColor
    }
}

