//凤凰登录baseUrl
export const fhBaseUrl = "https://w3m.huawei.com/"
//测试环境
export const testBaseUrl = "https://devecotesting-service.huawei.com/weekly/"
//内部正式环境
export const interBaseUrl = "https://devecotesting-service.huawei.com/"
//外部正式环境
export const outerBaseUrl = "https://devecotesting.huawei.com/"

export const token = "eyJUeXBlIjoiSnd0IiwidHlwIjoiSldUIiwiYWxnIjoiSFMyNTYifQ.eyJleHAiOjE2ODIzOTA3NzEsInVzZXJJZCI6MTAzLCJ0aW1lc3RhbXAiOjE2ODIzOTA3NzEwNjR9.2aSNrzPf-YX8bmMX_aeIPXkudZYCA2vQIR52Grnq4e4"

export const readTimeout = 30000

export const connectTimeout = 30000

export enum NetInterface {
    // TODO 外部用户 WebView登录接口 ("Content-Type: application/x-www-form-urlencoded")
    POST_OUTER_GET_TOKEN = "api/tokens",
    // TODO 获取用户信息
    GET_USER_INFO = "api/current_user",
    // TODO 创建任务
    POST_CREATE_TASK = "api/projects/{projectId}/testtasksvr/task/clientCreateTask",

    POST_UPLOAD = "storage/uploadOriginalFile",
    // TODO 上传任务结果
    POST_UPLOAD_RESULT = "api/projects/{projectId}/testtasksvr/task/uploadTaskResult",
    // 打点
    POST_TRACE = "api/tracker/trace",

    // TODO 凤凰登录     ("Content-Type: application/x-www-form-urlencoded;charset=UTF-8")
    POST_FH_LOGIN = "mcloud/mag/LoginWithSFV2",
    // TODO 凤凰登录 发送验证码 ("Content-Type: application/x-www-form-urlencoded;charset=UTF-8")
    POST_FH_SEF_SUID = "mcloud/mag/GetSFBySuid",

    // 凤凰登录 确认登陆 ("Content-Type: application/x-www-form-urlencoded;charset=UTF-8")
    POST_FH_CONFIRM = "mcloud/mag/ConfirmSFBySuid",

    // 内部获取token
    POST_INTER_GET_TOKEN = "api/sp/tokens",
    // hms获取token
    POST_HMS_GET_TOKEN = "api/sp2/tokens",
    // 鉴权打点 通过
    LICENSE_SUCCESS_TAKE_CARE = "802008110",
    // 鉴权打点 不通过
    LICENSE_FAIL_TAKE_CARE = "802008111",
    // 登录打点
    LOGIN_TAKE_CARE = "806001001",
    // 创建任务打点
    TASK_TAKE_CARE = "806001002",
}