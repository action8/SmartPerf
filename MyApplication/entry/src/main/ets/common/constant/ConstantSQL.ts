/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export const dbName = "gp.db"

export const dbVersion = 1

export const sql_t_general_info =
    "CREATE TABLE IF NOT EXISTS " + "t_general_info" + "(" +
    "sessionId TEXT PRIMARY KEY, " +
    "appName TEXT," +
    "appVersion TEXT," +
    "batteryVolume TEXT," +
    "board TEXT," +
    "brand TEXT," +
    "cpuCluster TEXT," +
    "dataSource TEXT," +
    "deviceName TEXT," +
    "deviceTypeName TEXT," +
    "endTime TEXT," +
    "packageName TEXT," +
    "plat TEXT," +
    "projectId TEXT," +
    "resolution TEXT," +
    "screenBrightness TEXT," +
    "sn TEXT," +
    "spVersion TEXT," +
    "startTime TEXT," +
    "taskId TEXT NOT NULL,"+
    "taskName TEXT," +
    "testCase TEXT," +
    "validTimes TEXT," +
    "testType TEXT," +
    "upStatus TEXT," +
    "uploadTaskId TEXT," +
    "user TEXT," +
    "userName TEXT," +
    "version TEXT," +
    "volume TEXT," +
    "test TEXT," +
    "testDuration TEXT," +
    "userId TEXT," +
    "isOnline TEXT," +
    "deviceNum TEXT," +
    "roomNumber TEXT," +
    "taskType TEXT" +

    ")"

export const sql_t_index_info = "CREATE TABLE IF NOT EXISTS " + "t_index_info" + "(" +
"timestamp TEXT PRIMARY KEY, " +
"taskId TEXT NOT NULL, " +
"ambientTemp TEXT," +
"shellBackTemp TEXT," +
"socThermalTemp TEXT," +
"batteryTemp TEXT," +
"gpuTemp TEXT," +
"shellFrontTemp TEXT," +
"shellFrameTemp TEXT," +
"systemHTemp TEXT," +
"ddrFrequency TEXT," +
"fps TEXT," +
"fpsJitters TEXT," +
"jank TEXT," +
"flag TEXT, " +
"caton TEXT," +
"generalCatonNum TEXT," +
"criticalCatonNum TEXT," +
"fatalCatonNum TEXT," +
"cpu0Frequency TEXT," +
"cpu0Load TEXT," +
"cpu1Frequency TEXT," +
"cpu1Load TEXT," +
"cpu2Frequency TEXT," +
"cpu2Load TEXT," +
"cpu3Frequency TEXT," +
"cpu3Load TEXT," +
"cpu4Frequency TEXT," +
"cpu4Load TEXT," +
"cpu5Frequency TEXT," +
"cpu5Load TEXT," +
"cpu6Frequency TEXT," +
"cpu6Load TEXT," +
"cpu7Frequency TEXT," +
"cpu7Load TEXT," +
"cpu8Frequency TEXT," +
"cpu8Load TEXT," +
"cpu9Frequency TEXT," +
"cpu9Load TEXT," +
"cpu10Frequency TEXT," +
"cpu10Load TEXT," +
"cpu11Frequency TEXT," +
"cpu11Load TEXT," +
"cpuLoad TEXT," +
"gpuLoad TEXT," +
"gpuFrequency TEXT," +
"enableHiz TEXT," +
"voltageNow TEXT," +
"currentNow TEXT," +
"capacity TEXT," +
"status TEXT," +
"pss TEXT," +
"cpu1ClusterFre TEXT," +
"cpu2ClusterFre TEXT," +
"cpu3ClusterFre TEXT," +
"cacheMisses TEXT," +
"instructions TEXT," +
"gpuCycles TEXT," +
"vertexComputeCycles TEXT," +
"fragmentCycles TEXT," +
"tilerCycles TEXT," +
"vertexComputeJobs TEXT," +
"fragmentJobs TEXT," +
"pixels TEXT," +
"earlyZTests TEXT," +
"earlyZKilled TEXT," +
"externalMemoryReadAccesses TEXT," +
"externalMemoryWriteAccesses TEXT," +
"externalMemoryReadBytes TEXT," +
"externalMemoryWriteBytes TEXT," +
"cacheWriteLookups TEXT," +
"cacheReadLookups TEXT," +
"externalMemoryWriteStalls TEXT," +
"externalMemoryReadStalls TEXT," +
"shaderCycles TEXT," +
"shaderArithmeticCycles TEXT," +
"shaderLoadStoreCycles TEXT," +
"shaderTextureCycles TEXT," +
"clocksSecond TEXT," +
"gpuUtilization TEXT," +
"gpuBusBusy TEXT," +
"verticesShadedSecond TEXT," +
"fragmentsShadedSecond TEXT," +
"texturesVertex TEXT," +
"texturesFragment TEXT," +
"aluVertex TEXT," +
"aluFragment TEXT," +
"timeShadingFragments TEXT," +
"timeShadingVertices TEXT," +
"timeCompute TEXT," +
"readTotal TEXT," +
"writeTotal TEXT," +
"textureMemoryReadBW TEXT," +
"vertexMemoryRead TEXT," +
"spMemoryRead TEXT," +
"qpGPUFrequency TEXT," +
"currNetworkType TEXT," +
"networkUpSpeed TEXT," +
"networkDownSpeed TEXT," +
"wlanSingleIntensity TEXT," +
"radioSingleIntensity TEXT," +
"networkDelay_SDK TEXT," +
"hostFPS TEXT," +
"deviceFPS TEXT," +
"deviceDrawCallTime TEXT," +
"hostDrawCallTimes TEXT," +
"effectQuality TEXT," +
"effectQualityMax TEXT," +
"shadowQuality TEXT," +
"shadowQualityMax TEXT," +
"grass TEXT," +
"grassMax TEXT," +
"glow TEXT," +
"glowMax TEXT," +
"bloom TEXT," +
"bloomMax TEXT," +
"msaa TEXT," +
"msaaMax TEXT," +
"gpuss_0_stepTemp TEXT," +
"gpuss_1_stepTemp TEXT," +
"cpu_0_0_stepTemp TEXT," +
"cpu_0_1_stepTemp TEXT," +
"cpu_0_2_stepTemp TEXT," +
"cpu_0_3_stepTemp TEXT," +
"cpu_1_0_stepTemp TEXT," +
"cpu_1_1_stepTemp TEXT," +
"cpu_1_2_stepTemp TEXT," +
"cpu_1_3_stepTemp TEXT," +
"cpu_1_4_stepTemp TEXT," +
"cpu_1_5_stepTemp TEXT," +
"cpu_1_6_stepTemp TEXT," +
"cpu_1_7_stepTemp TEXT" +
")"

export const task_powerSensor_info =
    "CREATE TABLE IF NOT EXISTS " + "task_powersensor_info" + "(" +
    "id TEXT PRIMARY KEY," +
    "taskId TEXT NOT NULL," +
    "sensor TEXT," +
    "power TEXT," +
    "current TEXT," +
    "percent TEXT" +
    ")"

export const task_powerApp_info =
    "CREATE TABLE IF NOT EXISTS " + "task_powerapp_info" + "(" +
    "id TEXT PRIMARY KEY," +
    "taskId TEXT NOT NULL," +
    "process TEXT," +
    "energy TEXT," +
    "percent TEXT" +
    ")"

export const task_powerThread_info =
    "CREATE TABLE IF NOT EXISTS" + "task_powerthread_info" + "(" +
    "id TEXT PRIMARY KEY," +
    "taskId TEXT NOT NULL," +
    "process TEXT," +
    "energy TEXT," +
    "percent TEXT" +
    ")"

export const sql_t_user_info =
    "CREATE TABLE IF NOT EXISTS " + "t_user_info" + "(" +
    "token TEXT," +
    "userId TEXT," +
    "user TEXT," +
    "projectId TEXT," +
    "loginTime TEXT" +
    ")"

export const sql_t_perform_info =
    "CREATE TABLE IF NOT EXISTS" + "t_perform_info" + "(" +
    "id TEXT," +
    "name TEXT," +
    "type TEXT," +
    "package TEXT," +
    "value TEXT," +
    "startTime TEXT," +
    "endTime TEXT" +
    ")"

export const sql_t_user_info_delete = "DELETE FROM t_user_info"
export const sql_t_scene_info_cal =
  "    select" +
  "            ROUND(AVG((CASE WHEN currentNow < 0 THEN (-1 * currentNow) ELSE currentNow END)),2) as averageCurrentNow," +
  "            ROUND(AVG((CASE WHEN (voltageNow * currentNow) < 0 THEN (voltageNow * currentNow)/(-3.8) ELSE (voltageNow * currentNow)/(3.8) END)),2) as electricity," +
  "            ROUND(MAX(systemHTemp),2) as maxSystemTem," +
  "            ROUND(MAX(systemHTemp) - MIN(systemHTemp),2) as systemTemRise," +
  "            ROUND(MAX(shellFrameTemp),3) as maxShellTemp," +
  "            ROUND(MAX(shellFrameTemp) - MIN(shellFrameTemp),3) as shellTemRise," +
  "            ROUND(avg(cpu${0}Frequency/1000),3) as cpuC1Frequency," +
  "            ROUND(avg(cpu${1}Frequency/1000),3) as cpuC2Frequency," +
  "            ROUND(avg(cpu${2}Frequency/1000),3) as cpuC3Frequency," +
  "            ROUND(avg(gpuFrequency/1000000),3) as gpuFrequency," +
  "            ROUND(avg(ddrFrequency/1000000),3) as ddrFrequency" +
  "    from `t_index_info`"

export const sql_t_FrameRate_info =
  "CREATE TABLE IF NOT EXISTS" + "t_FrameRate_info" + "(" +
    "id TEXT," +
    "name TEXT," +
    "type TEXT," +
    "package TEXT," +
    "value TEXT," +
    "startTime TEXT," +
    "endTime TEXT" +
    ")"