/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import wm from '@ohos.window'
import SPLogger from '../../utils/SPLogger'

const TAG = "FloatWindowFun"

export class FloatWindowFun {
    static floatingWindowOffsetX: number = 50
    static floatingWindowOffsetY: number = 300
    static titleWindowOffsetX: number = 300
    static titleWindowOffsetY: number = 200
    static lineChartWindowOffsetX: number= 700
    static lineChartWindowOffsetY: number= 200
    static windowWidth: number = 2560
    static windowHeight: number = 1600
    static atWidth: number =  95
    static initAllFun() {
        globalThis.CreateFloatingWindow = (() => {
            //5.5SP2  2106 改成 8
            let config = { name: "sp_floatingWindow", windowType: wm.WindowType.TYPE_FLOAT, ctx: globalThis.abilityContext };
            wm.createWindow(config).then((floatWin) => {
                floatWin.moveWindowTo(this.floatingWindowOffsetX, this.floatingWindowOffsetY).then(() => {
                    floatWin.resize(this.atWidth*globalThis.coefficient, this.atWidth*globalThis.coefficient).then(() => {
                        floatWin.getProperties().then((property) => {
                            property.isTransparent = false
                        })
                        floatWin.setUIContent('pages/FloatBall').then(() => {
                            floatWin.setWindowBackgroundColor("#00000000")//透明
                                floatWin.showWindow().then(() => {
                                    globalThis.showFloatingWindow = true
                                })
                        })
                    })
                })
            })
        });
        globalThis.CreateControlWindow = (() => {
            //5.5SP2  2106 改成 8
            let config = { name: "sp_controlWindow", windowType: wm.WindowType.TYPE_FLOAT, ctx: globalThis.abilityContext };
            wm.createWindow(config).then((floatWin) => {
                floatWin.moveWindowTo(this.floatingWindowOffsetX + 300, this.floatingWindowOffsetY - 150).then(() => {
                    floatWin.resize(this.atWidth*globalThis.coefficient*4, this.atWidth*globalThis.coefficient*1.5).then(() => {
                        floatWin.getProperties().then((property) => {
                            property.isTransparent = false
                        })
                        floatWin.setUIContent('pages/ControlBall').then(() => {
                            floatWin.setWindowBackgroundColor("#00000000")//透明
                            floatWin.showWindow().then(() => {
                                globalThis.showFloatingWindow = true
                            })
                        })
                    })
                })
            })
        });
        globalThis.MoveFloatingWindow = ((offsetX: number, offsetY: number) => {
            var xx = (this.floatingWindowOffsetX + offsetX * 2) < 0 ? 0 : ((this.floatingWindowOffsetX + offsetX * 2) > (this.windowWidth - 200) ? (this.windowWidth - 200) : (this.floatingWindowOffsetX + offsetX * 2))
            var yy = (this.floatingWindowOffsetY + offsetY * 2) < 0 ? 0 : ((this.floatingWindowOffsetY + offsetY * 2) > (this.windowHeight - 200) ? (this.windowHeight - 200) : (this.floatingWindowOffsetY + offsetY * 2))
            let fltWin = wm.findWindow("sp_floatingWindow")
            fltWin.moveWindowTo(xx,yy)

        })

        globalThis.SetFloatingWindowPosition = ((offsetX: number, offsetY: number) => {
            this.floatingWindowOffsetX = (this.floatingWindowOffsetX + offsetX * 2) < 0 ? 0 : ((this.floatingWindowOffsetX + offsetX * 2) > (this.windowWidth - 200) ? (this.windowWidth - 200) : (this.floatingWindowOffsetX + offsetX * 2))
            this.floatingWindowOffsetY = (this.floatingWindowOffsetY + offsetY * 2) < 0 ? 0 : ((this.floatingWindowOffsetY + offsetY * 2) > (this.windowHeight - 200) ? (this.windowHeight - 200) : (this.floatingWindowOffsetY + offsetY * 2))
        })

        globalThis.DestroyFloatingWindow = (() => {
            let fltWin = wm.findWindow("sp_floatingWindow")
            fltWin.destroyWindow().then(() => {
                globalThis.showFloatingWindow = false
            })

        })

        globalThis.CreateTitleWindow = (() => {
            let config = { name: "sp_TitleWindow", windowType: wm.WindowType.TYPE_FLOAT, ctx: globalThis.abilityContext };
            wm.createWindow(config).then((floatWin) => {
                floatWin.moveWindowTo(this.titleWindowOffsetX, this.titleWindowOffsetY).then(() => {
                    floatWin.resize(350*globalThis.coefficient, 500*globalThis.coefficient).then(() => {
                        floatWin.getProperties().then((property) => {
                            property.isTransparent = false
                        })
                        floatWin.setUIContent('pages/TitleWindowPage').then(() => {
                            floatWin.setWindowBackgroundColor("#00000000")
                            floatWin.hide()
                            SPLogger.DEBUG(TAG, 'CreateTitleWindow Done')
                        })
                    })
                })
            })
        })

        globalThis.MoveTitleWindow = ((offsetX: number, offsetY: number) => {
            var xx = (this.titleWindowOffsetX + offsetX * 2) < 0 ? 0 : ((this.titleWindowOffsetX + offsetX * 2) > (this.windowWidth - 500) ? (this.windowWidth - 500) : (this.titleWindowOffsetX + offsetX * 2))
            var yy = (this.titleWindowOffsetY + offsetY * 2) < 0 ? 0 : ((this.titleWindowOffsetY + offsetY * 2) > (this.windowHeight - 330) ? (this.windowHeight - 330) : (this.titleWindowOffsetY + offsetY * 2))
            let fltWin = wm.findWindow("sp_TitleWindow")
            fltWin.moveWindowTo(xx, yy)

        })

        globalThis.SetTitleWindowPosition = ((offsetX: number, offsetY: number) => {
            this.titleWindowOffsetX = (this.titleWindowOffsetX + offsetX * 2) < 0 ? 0 : ((this.titleWindowOffsetX + offsetX * 2) > (this.windowWidth - 500) ? (this.windowWidth - 500) : (this.titleWindowOffsetX + offsetX * 2))
            this.titleWindowOffsetY = (this.titleWindowOffsetY + offsetY * 2) < 0 ? 0 : ((this.titleWindowOffsetY + offsetY * 2) > (this.windowHeight - 330) ? (this.windowHeight - 330) : (this.titleWindowOffsetY + offsetY * 2))
        })

        globalThis.DestroyTitleWindow = (() => {
            let fltWin = wm.findWindow("sp_TitleWindow")
            fltWin.destroyWindow()
        })

        globalThis.HideTitleWindow = (() => {
            let fltWin = wm.findWindow("sp_TitleWindow")
            fltWin.hide()
        })

        globalThis.ShowTitleWindow = (() => {
            let fltWin = wm.findWindow("sp_TitleWindow")
            fltWin.showWindow()

        })

        // 帧率类
        globalThis.CreateFrameRateWindow = (() => {
            let config = { name: "sp_FrameRateWindow", windowType: wm.WindowType.TYPE_FLOAT, ctx: globalThis.abilityContext };
            wm.createWindow(config).then((floatWin) => {
                floatWin.moveWindowTo(this.floatingWindowOffsetX + 100, this.floatingWindowOffsetY - 150).then(() => {
                    floatWin.resize(this.atWidth * globalThis.coefficient * 6, this.atWidth * globalThis.coefficient *1.5).then(() => {
                        floatWin.getProperties().then((property) => {
                            property.isTransparent = false
                        })
                        floatWin.setUIContent('pages/FrameRateBall').then(() => {
                            floatWin.setWindowBackgroundColor("#00000000")
                            floatWin.showWindow().then(()=>{
                                globalThis.showFloatingWindow = true
                            })
                        })
                    })
                })
            })
        })

    }
}

