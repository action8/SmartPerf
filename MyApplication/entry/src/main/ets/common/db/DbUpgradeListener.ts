import { DbUtils } from "../utils/DbUtils"

export interface DbUpgradeListener{
    onUpgrade(db: DbUtils, oldVersion:number, newVersion: number);
}