import relationalStore from "@ohos.data.relationalStore"

export class Selector {
    public predicates
    private tableName: string;
    private queryColumns: Array<string>
    private constructor(tableName: string, columns: Array<string>){
        this.tableName = tableName;
        this.queryColumns = columns;
        this.predicates = new relationalStore.RdbPredicates(this.tableName)
    }
    public static from(tableName: string, columns: Array<string>): Selector {
        return new Selector(tableName, columns);
    }
    public where(columnName: string, op: string, value: number|string|boolean): Selector {
        switch (op) {
            case "equalTo":
                this.predicates.equalTo(columnName,value)
                break
            case "notEqualTo":
                this.predicates.notEqualTo(columnName, value)
                break
        }
        return this;
    }
    public and(columnName:string, op: string, value: number|string|boolean): Selector {
        switch (op) {
            case "equalTo":
                this.predicates.and().equalTo(columnName, value)
                break
            case "notEqualTo":
                this.predicates.and().notEqualTo(columnName, value)
                break
        }
        return this;
    }
    public or(columnName:string, op: string, value: number|string|boolean): Selector {
        switch (op) {
            case "equalTo":
                this.predicates.or().equalTo(columnName, value)
                break
            case "notEqualTo":
                this.predicates.or().notEqualTo(columnName, value)
                break
        }
        return this;
    }
    public orderByDesc(columnName:string): Selector{
        this.predicates.or().orderByDesc(columnName)
        return this;
    }
    public getQueryColumns(): Array<string> {
        return this.queryColumns
    }
}