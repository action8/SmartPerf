import { DbUtils } from "../../utils/DbUtils"

export class SqlInfoBuilder {
    public static buildCreateTableSqlInfo(db: DbUtils): string {
        return "CREATE TABLE IF NOT EXISTS" + db.getDaoConfig().getTableName() + "(" +
                db.getDaoConfig().getCreateTableSql() + ")";
    }
}