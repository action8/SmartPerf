export abstract class QueryCallBack {
    public abstract onSuccessCreateDb();
    public abstract onSuccessCreateTable();
    public abstract onSuccessInsert(ret: any);
    public abstract onErrorInsert(err: any);
    public abstract onSuccessQuery(resultSet: any);
    public abstract onSuccessQueryFirst(map: Map<string, any>);
    public abstract onSuccessUpdate(ret: any);
    public abstract onErrorUpdate(err: any);
    public abstract onSuccessDelete(rows: any);
    public abstract onErrorDelete(err: any);
    public abstract onSuccessDropTable();
    public abstract onSuccessDeleteDb();
}