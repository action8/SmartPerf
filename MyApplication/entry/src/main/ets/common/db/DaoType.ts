export enum DaoType {
    CREATE_STORE = "create_store",
    CREATE_TABLE = "create_table",
    INSERT = "insert",
    QUERY = "query",
    QUERY_FIRST = "queryFirst",
    UPDATE= "update",
    DELETE = "delete",
    DROPTABLE= "dropTable",
}