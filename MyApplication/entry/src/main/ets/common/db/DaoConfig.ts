import { DbUpgradeListener } from "./DbUpgradeListener"

export class DaoConfig {
    private dbName = "xUtils.db";
    private tableName: string;
    private dbVersion = 1;
    private dbUpgradeListener: DbUpgradeListener;
    private dbDir: string;
    private createTableSql: string;

    public getDbName(): string {
        return this.dbName
    }
    public setDbName(dbName: string) {
        this.dbName = dbName
    }
    public getTableName(): string {
        return this.tableName
    }
    public setTableName(tableName: string) {
        if(tableName != null){
            this.tableName = tableName;
        }
    }
    public getDbVersion(): number {
        return this.dbVersion
    }
    public setDbVersion(dbVersion: number) {
        this.dbVersion = dbVersion
    }
    public getDbUpgradeListener(): DbUpgradeListener {
        return this.dbUpgradeListener
    }
    public setDbUpgradeListener(dbUpgradeListener: DbUpgradeListener){
        this.dbUpgradeListener = dbUpgradeListener
    }
    public getDbDir(): string {
        return this.dbDir
    }
    public setDbDir(dbDir: string) {
        this.dbDir = dbDir
    }
    public getCreateTableSql(): string {
        return this.createTableSql
    }
    public setCreateTableSql(createTableSql: string) {
        this.createTableSql = createTableSql
    }
}