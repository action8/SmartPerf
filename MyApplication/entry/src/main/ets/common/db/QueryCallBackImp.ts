import { QueryCallBack } from "./QueryCallBack"

export class DbCallBackImp extends QueryCallBack{
    onSuccessCreateDb(){
        console.log('DbUtils: create store done')
    }

    onSuccessCreateTable(){

    }

    onSuccessInsert(ret: any){

    }

    onErrorInsert(err: any){

    }

    onSuccessUpdate(){

    }

    onErrorUpdate(err: any){

    }

    onSuccessDelete(rows: any){

    }
    onErrorDelete(err: any){

    }

    onSuccessQuery(resultSet: any){

    }

    onSuccessQueryFirst(map: Map<string, any>){

    }

    onSuccessDropTable(){

    }

    onSuccessDeleteDb(){

    }
}