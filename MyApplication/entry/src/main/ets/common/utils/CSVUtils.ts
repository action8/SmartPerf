/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { TGeneralInfo, SceneInfo, TPowerSensorInfo } from '../entity/DatabaseEntity';
import { TIndexInfo } from '../entity/DatabaseEntity';

//垂直生成 TGenneralInfo to string
export function csvGeneralInfo(tGeneralInfo: TGeneralInfo): string {
    let data = ""
    for (let k of Object.keys(tGeneralInfo)) {
        data += k + "," + tGeneralInfo[k] + "\n"
    }
    return data
}

//水平生成 TSceneInfo to string
export function csvTSceneInfo(SceneInfos: Array<SceneInfo>): string {
    let tittle = csvTSceneInfoTittle()
    let data = ""
    for (var index = 0; index < SceneInfos.length; index++) {
        const t = SceneInfos[index];
        for (let k of Object.keys(t)) {
            data += t[k] + ","
        }
        data = data.substring(0, data.lastIndexOf(","))
        data += "\n"
    }
    let result = tittle + data
    return result
}

//水平生成 TSceneInfo TITTLE to string
export function csvTSceneInfoTittle(): string {
    var sceneInfo:SceneInfo = new SceneInfo()
    let data = ""
    for ( let k of Object.keys(sceneInfo)) {
        data += k + ","
    }
    data = data.substring(0, data.lastIndexOf(","))
    data += "\n"
    return data
}

//水平生成 TIndexInfo to string
export function csvTIndexInfo(tIndexInfos: Array<TIndexInfo>): string {
    let tittle = csvTIndexInfoTittle()
    let data = ""
    for (var index = 0; index < tIndexInfos.length; index++) {
        const t = tIndexInfos[index];
        for (let k of Object.keys(t)) {
            data += t[k] + ","
        }
        data = data.substring(0, data.lastIndexOf(","))
        data += "\n"
    }
    let result = tittle + data
    return result
}

//水平生成 TPowerSensorInfo CONTENT to string
export function csvTPowerSensorInfo(powerSensorInfos: Array<TPowerSensorInfo>): string {
    let tittle = csvTPowerSensorInfoTittle()
    let data = ""
    for (var index = 0; index < powerSensorInfos.length; index++) {
        const t = powerSensorInfos[index];
        for (let k of Object.keys(t)) {
            data += t[k] + ","
        }
        data += "\n"
    }
    let headStr = data.substring(0, data.indexOf(","))
    let result = tittle + data
    return result
}

//水平生成 TIndexInfo TITTLE to string
export function csvTIndexInfoTittle(): string {
    var tIndexInfo: TIndexInfo = new TIndexInfo()
    let data = ""
    for (let k of Object.keys(tIndexInfo)) {
        data += k + ","
    }
    data = data.substring(0, data.lastIndexOf(","))
    data += "\n"
    return data
}

//水平生成 TPowerSensorInfo TITTLE to string
export function csvTPowerSensorInfoTittle(): string {
    var powerSensorInfo: TPowerSensorInfo = new TPowerSensorInfo()
    let data = ""
    for (let k of Object.keys(powerSensorInfo)) {
        data += k + ","
    }
    data = data.substring(0, data.lastIndexOf(","))
    data += "\n"
    return data
}





