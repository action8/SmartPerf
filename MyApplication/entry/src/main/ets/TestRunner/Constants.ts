export class Constants {
    static readonly APP_LIST = "app_list";
    static readonly DESK_BUNDLE_INFO = "desk_bundle_info";
    static readonly DESK_APP_LIST = "desk_app_list";
    static readonly GET_APP_VERSION = "get_app_version";
    static readonly GO_BACK = "go_back";
    static readonly GO_HOME = "go_home";
}