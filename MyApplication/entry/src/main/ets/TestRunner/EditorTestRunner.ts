import TestRunner from '@ohos.application.testRunner'
import AbilityDelegatorRegistry from '@ohos.app.ability.abilityDelegatorRegistry'
import BundleManager from '../../../main/ets/common/utils/BundleMangerUtils'
import { Constants } from './Constants'

var abilityDelegator = undefined
var abilityDelegatorArguments = undefined
const FIN_TEST_OK = 0
const FIN_TEST_ERR = -1

export default class EditorTestRunner implements TestRunner {
    constructor(){}

    async onPrepare() {
        abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator()
        abilityDelegatorArguments = AbilityDelegatorRegistry.getArguments()
        globalThis.abilityContext = abilityDelegator.getAppContext()
        let testType = abilityDelegatorArguments.parameters['-s type']
        if (testType == Constants.DESK_APP_LIST) {
            await BundleManager.getAppList()
            abilityDelegator.finishTest("ok", FIN_TEST_OK, (err) => {
                console.log("EditorTestRunner finishTest" + JSON.stringify(err))
            })
        }
        if (testType == Constants.GET_APP_VERSION) {
            // await BundleManager.getAppList()
            abilityDelegator.print("SmartPerf Version:1.0.2", (err) => {
                console.log("EditorTestRunner finishTest" + JSON.stringify(err))
            })
        }
    }

    async onRun() {
        abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator()
    }
}