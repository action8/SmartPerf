import FloatWindowConstant from '../common/ui/floatwindow/FloatWindowConstant'
import { hideFloatWindow, moveFloatWindow, setFloatWindow } from '../common/ui/floatwindow/utils/FloatWindowUtils'

@Preview
@Component
@Entry
struct ChartWindowPage {
  private settings: RenderingContextSettings = new RenderingContextSettings(true)
  private context: CanvasRenderingContext2D = new CanvasRenderingContext2D(this.settings)
  @State title: string = "SmartPerf"
  private xyColor = "rgb(255, 255, 255)"
  private XScale: number = 8 // x轴波形偏移量
  private YScale: number = 1 // y轴波形偏移量
  private XLength: number = 168 //X轴长度
  private YLength: number = 105  //Y轴长度
  private scalesTop: number = 2000 //刻度最值
  private floatType: FloatWindowConstant
  @State unitType: string = ""
  @State floatName: string = ""
  private floatWindowName: string = ""
  private intervalWave = -1
  @State data: number[]= [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]   //数据集合
  @State currentValue: number = 0 //数值

  offsetX: number = -1    //悬浮框移动触摸点 X
  offsetY: number = -1   //悬浮框移动触摸点 X

  aboutToAppear() {
    let storage = LocalStorage.GetShared()
    this.floatWindowName = storage.get("storage_float_name")
    this.floatType = storage.get("storage_float_type")
    this.calcScales()
  }
  aboutToDisappear() {
    if (this.intervalWave != -1) {
      clearInterval(this.intervalWave)
    }
  }
  onDataChange() {
    if (FloatWindowConstant.CURRENT_NOW == this.floatType) {
      this.currentValue = Number(globalThis.currentNow).valueOf()
      this.floatName = "电流"
    }
    if (FloatWindowConstant.SHELL_BACK_TEMP == this.floatType) {
      this.currentValue = Number(globalThis.shellBackTemp).valueOf()
      this.floatName = "壳温"
    }
    if (FloatWindowConstant.FPS == this.floatType) {
      this.currentValue = Number(globalThis.lineFps).valueOf()
      this.floatName = "帧率"
    }
    if (FloatWindowConstant.CPU0_FREQUENCY == this.floatType) {
      this.currentValue = (Number(globalThis.cpu0Frequency) / 1e3).valueOf()
      this.floatName = "cpu-A频率"
    }
    if (FloatWindowConstant.CPU1_FREQUENCY == this.floatType) {
      this.currentValue = (Number(globalThis.cpu1Frequency) / 1e3).valueOf()
      this.floatName = "cpu-B频率"
    }
    if (FloatWindowConstant.CPU2_FREQUENCY == this.floatType) {
      this.currentValue = (Number(globalThis.cpu2Frequency) / 1e3).valueOf()
      this.floatName = "cpu-C频率"
    }
    if (FloatWindowConstant.DDR_FREQUENCY == this.floatType) {
      this.currentValue = (Number(globalThis.ddrFrequency) / 1e6).valueOf()
      this.floatName = "ddr频率"
    }
    if (FloatWindowConstant.GPU_FREQUENCY == this.floatType) {
      this.currentValue = (Number(globalThis.gpuFrequency) / 1e6).valueOf()
      this.floatName = "gpu频点"
    }
    if (FloatWindowConstant.RAM == this.floatType) {
      this.currentValue = Number(globalThis.pss).valueOf()
      this.floatName = "pss内存"
    }
    if (FloatWindowConstant.SOC == this.floatType) {
      this.currentValue = Number(globalThis.socThermalTemp).valueOf()
      this.floatName = "SOC温度"
    }
  }

  calcScales() {
    if (FloatWindowConstant.CURRENT_NOW == this.floatType) {
      this.scalesTop = 2000
      this.unitType = "mA"
    }
    if (FloatWindowConstant.SHELL_BACK_TEMP == this.floatType) {
      this.scalesTop = 100
      this.unitType = "℃"
    }
    if (FloatWindowConstant.SOC == this.floatType) {
      this.scalesTop = 100
      this.unitType = "℃"
    }
    if (FloatWindowConstant.FPS == this.floatType) {
      this.scalesTop = 144
      this.unitType = "FPS"
    }
    if (FloatWindowConstant.CPU0_FREQUENCY == this.floatType ||
    FloatWindowConstant.CPU1_FREQUENCY == this.floatType ||
    FloatWindowConstant.CPU2_FREQUENCY == this.floatType) {
      this.scalesTop = 3000
      this.unitType = "MHZ"
    }
    if (FloatWindowConstant.DDR_FREQUENCY == this.floatType) {
      this.scalesTop = 2000
      this.unitType = "MHZ"
    }
    if (FloatWindowConstant.GPU_FREQUENCY == this.floatType) {
      this.scalesTop = 1000
      this.unitType = "MHZ"
    }
    if (FloatWindowConstant.RAM == this.floatType) {
      this.scalesTop = 1000000
      this.unitType = "KB"
    }
    this.YScale = this.YLength / this.scalesTop
  }
  MoveWindow(offsetX: number, offsetY: number) {
    moveFloatWindow(this.floatWindowName, offsetX, offsetY)
  }
  SetWindowPosition(offsetX: number, offsetY: number) {
    setFloatWindow(offsetX, offsetY)
  }

  build() {
    Stack({ alignContent: Alignment.Top }) {
      Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Start, justifyContent: FlexAlign.Center}) {
        Flex({ justifyContent: FlexAlign.SpaceBetween}) {
          Text(this.floatName  + ":" )
            .fontSize('10fp')
            .fontColor($r('app.color.color_fff'))
            .margin({ left: 5, top: 5 }) //文本显示
          Text( this.currentValue + this.unitType)
            .fontSize('16fp')
            .fontColor('#FF0000')
            .fontWeight(5)
            .margin({ left: 1,top: 1 }) //文本显示
          Image($r("app.media.icon_close_small")).width('12vp').height('12vp').backgroundColor(Color.White).onClick(() => {
            hideFloatWindow(this.floatWindowName)

          })
        }
        .height('18vp').width('100%')
        Canvas(this.context)
          .width('100%')
          .height('100%')
          .onReady(() => {
            let xStartPoint = 20
            let yStartPoint = 10
            this.intervalWave = setInterval(() => {
              this.context.strokeStyle = this.xyColor
              this.context.fillStyle = this.xyColor
              this.context.font = '18px sans-serif'
              if (this.scalesTop > 100000) {
                this.context.fillText((this.scalesTop / 1e4).toString() + 'w', xStartPoint -15, yStartPoint + 2)
                this.context.fillText((this.scalesTop / 1e4 / 2).toString() + 'w', xStartPoint -15, yStartPoint + this.YLength / 2)
              } else {
                this.context.fillText(this.scalesTop.toString(), xStartPoint -15, yStartPoint + 2)
                this.context.fillText((this.scalesTop / 2).toString(), xStartPoint -15, yStartPoint + this.YLength / 2)
              }
              this.context.fillText('0', xStartPoint - 5, yStartPoint + this.YLength)

              // x轴
              this.context.beginPath()
              this.context.strokeStyle = this.xyColor
              this.context.moveTo(xStartPoint, yStartPoint + this.YLength)
              this.context.lineTo(xStartPoint + this.XLength, yStartPoint + this.YLength)
              this.context.stroke()

              // y轴
              this.context.beginPath()
              this.context.strokeStyle = this.xyColor
              this.context.moveTo(xStartPoint, yStartPoint)
              this.context.lineTo(xStartPoint, yStartPoint + this.YLength)
              this.context.stroke()

              this.onDataChange()
              if (this.data.length >= 22) {
                this.data.shift()
              }
              if (this.currentValue == 0) {
                this.data.push(0)
              } else {
                let lineCount:number = this.currentValue * this.YScale
                this.data.push(Math.abs(lineCount))
              }
              // k线绘制
              if (this.data.length > 1 ) {
                this.context.clearRect(xStartPoint + 0.5, yStartPoint, this.XLength + 20, this.YLength -0.5)
                for (let i = 1; i < this.data.length; i++) {
                  this.context.beginPath()
                  this.context.strokeStyle = this.xyColor
                  this.context.moveTo(xStartPoint + 0.5 + (i - 1) *this.XScale, yStartPoint - this.data[i - 1] + this.YLength)
                  this.context.lineTo(xStartPoint + 0.5 + i * this.XScale, yStartPoint - this.data[i] + this.YLength)
                  this.context.stroke()
                }
              }
            }, 1000)
          })
      }
      .margin({ top:2, right: 2}).gesture(
      GestureGroup(GestureMode.Exclusive,
      PanGesture({})
        .onActionStart((event: GestureEvent) => {

        })
        .onActionUpdate((event: GestureEvent) => {
          this.offsetX = event.offsetX
          this.offsetY = event.offsetY
        })
        .onActionEnd(() => {
          this.MoveWindow(this.offsetX, this.offsetY)
          this.SetWindowPosition(this.offsetX,this.offsetY)
        })
      )
      )
    }
  }
}
